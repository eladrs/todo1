<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/customers/{id}', function ($id) {
    if(is_numeric($id))
        return view('pages.validValue',['id'=>$id]);
    
    else
        return view( "pages.invalidValue");
});
Route::resource('books', 'BooksController');

 